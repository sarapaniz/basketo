import 'package:bastream/Model/basket.dart';
import 'package:flutter/material.dart';

class BasketScreen extends StatefulWidget {
  const BasketScreen({Key? key}) : super(key: key);

  @override
  State<BasketScreen> createState() => _BasketScreenState();
}

class _BasketScreenState extends State<BasketScreen> {
  late Basket basket;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    basket = Basket.instance;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300,
      height: 300,
      color: Colors.blueGrey[200],
      child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: StreamBuilder(
            stream: basket.basketSC.stream,
            builder: (context, snapshot) {
                return ListView.builder(
                  itemCount: basket.items.length,
                  itemBuilder: (context, index) {
                    return Text(basket.items[index].title);
                  },
                );
            },
          )),
    );
  }
}
