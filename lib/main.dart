import 'dart:async';
import 'dart:math';

import 'package:bastream/home.dart';
import 'package:bastream/main_wrapper.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(Bastream());
}
class Bastream extends StatefulWidget {
  const Bastream({Key? key}) : super(key: key);

  @override
  State<Bastream> createState() => _BastreamState();
}

class _BastreamState extends State<Bastream> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MainWrapper(page_widget: HomeScreen(), page_title: "home")
    );
  }

}
/*
class Bastream extends StatefulWidget {
  const Bastream({Key? key}) : super(key: key);
  @override
  State<Bastream> createState() => _BastreamState();
}
class _BastreamState extends State<Bastream> {
  int randomDigit = 0;
  StreamController<int> streamController = StreamController<int>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    streamController.stream.listen((event) {
      setState(() {
        randomDigit = event;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(randomDigit.toString() ?? "", style: TextStyle(fontSize: 50)),
              ElevatedButton(
                onPressed: () => {
                    generateRandomDigit().listen((event) {
                    streamController.sink.add(event);
                  })
                },
                child: Text("Get Random Digit"),
              )
            ],
          ),
        ),
      ),
    );
  }

  Stream<int> generateRandomDigit() async* {
    int index = 0;
    while (index < 10) {
      await Future.delayed(Duration(seconds: 1));
      yield Random().nextInt(100);
      index++;
    }
  }
}
*/