import 'package:bastream/Model/basket.dart';
import 'package:bastream/basket.dart';
import 'package:bastream/main_wrapper.dart';
import 'package:flutter/material.dart';

import 'Model/cart_item.dart';

class StoreScreen extends StatefulWidget {
  const StoreScreen({Key? key}) : super(key: key);

  @override
  State<StoreScreen> createState() => _StoreScreenState();
}

class _StoreScreenState extends State<StoreScreen> {

  late Basket basket;

  List<CartItem> storeItems = [
    CartItem(1, "apple", 25000),
    CartItem(2, "orange", 15000),
    CartItem(3, "banana", 75000),
    CartItem(4, "berry", 50000),
    CartItem(5, "melon", 5000),
    CartItem(6, "tomato", 17000),
  ];



  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    basket = Basket.instance;
  }

  @override
  Widget build(BuildContext context) {
    return MainWrapper(
        page_widget: pageWidget(),
        page_title: "Store");
  }

  Widget pageWidget(){
    return Container(
      color: Colors.greenAccent,
      child: ListView(
        children: [
          for(var item in storeItems)
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: ElevatedButton(
                onPressed: (){
                  basket.addToBasket(item);
                  //print(basket.items.length);
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text("${item.title}", style: TextStyle(fontSize: 25)),
                    Text("${item.price}", style: TextStyle(fontSize: 25)),
                  ],
                ),
              ),
            ),
          BasketScreen(),
        ],
      ),
    );
  }
}

