import 'package:flutter/material.dart';

class MainWrapper extends StatelessWidget {
  Widget? page_widget;
  String? page_title;
  MainWrapper({Key? key, required this.page_widget, required this.page_title }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("${page_title}")),
      body: page_widget,
    );
  }
}
