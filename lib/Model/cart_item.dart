
class CartItem {

  int id;
  String title;
  int price;
  int count = 1;

  CartItem(this.id, this.title, this.price);
}
