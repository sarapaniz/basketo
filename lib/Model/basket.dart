import 'dart:async';

import 'cart_item.dart';

class Basket {
  Basket._();
  static final instance = Basket._();

  late StreamController<int> basketSC = StreamController<int>.broadcast();

  List<CartItem> _items = [];

  List<CartItem> get items => _items;

  // void dispose(){
  //   basketSC.close();
  // }

  addToBasket(CartItem item){
    _items.add(item);

    basketSC.sink.add(item.id);
  }

  removeFromBasket(CartItem item){
    _items.remove(item);

    basketSC.sink.add(item.id);

  }

}